#!/bin/bash

### create namespaces


sudo kubectl apply -f ./namespace.yml

### config map

sudo kubectl apply -f  ./configmap.yml

### create  services

sudo kubectl apply -f ./services.yml

### create deployment

sudo kubectl apply -f ./workloads.yml

### make dasboard file executable
sudo chmod +x ./start-dashboard.sh

### execute dashboard file
sudo sh ./start-dashboard.sh