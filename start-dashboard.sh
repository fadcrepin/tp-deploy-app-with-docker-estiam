#!/bin/bash

### launch the kubernetes dashboard

sudo minikube dashboard 

### authorise external connection on the dashboard

sudo kubectl proxy --address='0.0.0.0' --disable-filter=true 




