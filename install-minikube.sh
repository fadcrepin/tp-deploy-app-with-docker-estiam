#!/bin/bash

sudo apt-get update -y

sudo apt-get install ca-certificates software-properties-common -y

#installing curl
sudo apt-get install curl -y

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add –

sudo apt-get remove docker docker-engine docker.io -y

sudo apt install docker.io -y

sudo systemctl start docker

sudo systemctl enable docker

docker --version

#sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu  $(lsb_release -cs)  stable"
sudo apt-get update -y
 
#installing kubectl binary with curl
sudo curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
 
sudo curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.17.3/bin/linux/amd64/kubectl
 
#Make the kubectl binary executable
sudo chmod +x ./kubectl
 
#Move the binary in to your PATH
sudo mv ./kubectl /usr/local/bin/kubectl
 
sudo kubectl version --client
 
sudo apt-get update && sudo apt-get install -y apt-transport-https
 
sudo curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update -y
sudo apt-get install -y kubectl
 
sudo curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 \
  && chmod +x minikube
 
sudo mkdir -p /usr/local/bin/
sudo install minikube /usr/local/bin/
 
sudo minikube start --vm-driver=none
 
sudo minikube status

sudo chmod +x ./minikube-pods.sh

sudo sh ./minikube-pods.sh